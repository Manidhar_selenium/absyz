package com.rediff.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelUtils extends Common {

	public Object[][] readExcel() throws EncryptedDocumentException, IOException {
		InputStream fiss = new FileInputStream(path + "\\src\\main\\resources\\TestData.xlsx");
		Workbook workbook = WorkbookFactory.create(fiss);
		Sheet s = workbook.getSheetAt(0);

		int lastRowNo = s.getLastRowNum();

		int totalRows = lastRowNo + 1;
		System.out.println("totalRows: " + totalRows);
		Row row = s.getRow(0);
		int totalColuumns = row.getLastCellNum();
		System.out.println("totalColuumns: " + totalColuumns);
		Object o[][] = new Object[totalRows][totalColuumns];

		// rows
		for (int i = 0; i < totalRows; i++) {
			row = s.getRow(i);
			totalColuumns = row.getLastCellNum();
			// columns
			for (int j = 0; j < totalColuumns; j++) {
				Cell c = row.getCell(j);
				String ss = c.getStringCellValue();
				o[i][j] = ss;
			}
		}
		return o;
	}

	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		// TODO Auto-generated method stub
		ExcelUtils e = new ExcelUtils();
		e.readExcel();
	}

}
