package com.rediff.login;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rediff.common.Common;
import com.rediff.common.ExcelUtils;

public class Login extends Common {
//Comment from Computer2 on Login page
//Comment from Computer2 on Login page
	@BeforeMethod
	public void openBrowser() throws IOException {
		loadFiles();
		openBrowsers();
	}

	@Test(priority = 2, dependsOnMethods = "verifyPageTitle", dataProvider = "testData")
	public void invalidLogin(String username, String password) throws IOException {
		
		try {
			driver.findElement(By.xpath("//input[@id='login1']")).sendKeys(username);
			driver.findElement(By.id("password")).sendKeys(password);
			driver.findElement(By.name("proceed")).click();

			String actualErrorMessage = driver.findElement(By.id("div_login_error")).getText();
			String expectedErrorMessage = "Wrong username and password combination.";
			Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
			logger.debug("Validating invalid login with username: "+username+" and password as: "+password);
		} catch (Exception e) {
			e.printStackTrace();
			captureScreenshot();
			logger.error(e.toString());
		}
	}

	@Test(priority = 1)
	public void verifyPageTitle() throws IOException {
		try {
		String actualPageTitle = driver.getTitle();
		String expectedPageTitle = "Rediffmail";
		Assert.assertEquals(actualPageTitle, expectedPageTitle);
		logger.debug("Validating page title");
		}catch(Exception e) {
			e.printStackTrace();
			captureScreenshot();
			logger.error(e.toString());
		}
	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

	@DataProvider
	public Object[][] testData() throws EncryptedDocumentException, IOException {
		ExcelUtils eu=new ExcelUtils();
		return eu.readExcel();
	}

}
