package com.rediff.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class Common {
//Comment from Computer1
	InputStream fis = null;
	public Properties env = null;
	public WebDriver driver = null;
	String path=System.getProperty("user.dir");
	public static Logger logger=null;
	
	//@BeforeSuite
	public void loadFiles() throws IOException {
		logger=LogManager.getLogger(Common.class.getName());
		
		fis = new FileInputStream(
				path+"\\src\\main\\resources\\Environment.properties");
		env = new Properties();
		env.load(fis);
		logger.debug("Loading Property files");
	}

	//@AfterSuite
	public void clearFiles() throws IOException {
		fis.close();
		logger.debug("Clearing Property files");
	}

	public void openBrowsers() {
		String browserName=env.getProperty("browser");
		
		if(browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					path+"\\src\\main\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.get(env.getProperty("url"));
			logger.debug("Opened Chrome Browser");
		}else if(browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", path+"\\src\\main\\resources\\geckodriver.exe");
			WebDriver driver=new FirefoxDriver();
			driver.get(env.getProperty("url"));
			logger.debug("Opened Firefox Browser");
		}else if(browserName.equalsIgnoreCase("opera")) {
			System.setProperty("webdriver.opera.driver", path+"\\src\\main\\resources\\operadriver.exe");
			WebDriver driver=new OperaDriver();		
			driver.get(env.getProperty("url"));
			logger.debug("Opened Opera Browser");		
		}else if(browserName.equalsIgnoreCase("edge")) {
			System.setProperty("webdriver.edge.driver",  path+"\\src\\main\\resources\\msedgedriver.exe");
			WebDriver driver=new EdgeDriver();
			driver.get(env.getProperty("url"));
			logger.debug("Opened Edge Browser");
		}else {
			throw new Error("Please cross check the browser name given in Environment.properties");
		}
		
	}
	public void closeBrowsers(){
		driver.close();
		logger.debug("Closed browser");
	}
	public void captureScreenshot() throws IOException{
		TakesScreenshot screen=((TakesScreenshot)driver);
		File source=screen.getScreenshotAs(OutputType.FILE);
		File destination=new File(path+"\\screenshots\\Screen1.png");
		
		FileUtils.copyFile(source, destination);
		logger.debug("Captured screenshot");
	}

}
