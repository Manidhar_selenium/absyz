package com.rediff.createaccount;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.rediff.common.Common;

public class CreateAccount extends Common{

	@BeforeMethod
	public void openBrowser() throws IOException {
		loadFiles();
		openBrowsers();
	}

	@Test
	public void createAccountTest() throws InterruptedException, IOException {
		SoftAssert softAssertion=new SoftAssert();
		try {
		
		driver.findElement(By.linkText("Create a new account")).click();
		driver.findElement(By.xpath("//td[contains(text(),'Full Name')]/following-sibling::td[2]/input"))
				.sendKeys("rajesh");
		driver.findElement(
				By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/following-sibling::td[2]/input[1]"))
				.sendKeys("rajesh");
		driver.findElement(
				By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/following-sibling::td[2]/input[2]")).click();
		Thread.sleep(1500);
		String actualErrorMessage = driver.findElement(By.xpath("//div[@id='check_availability']/font/b")).getText();
		String expectedErrorMessage = "Sorry, the ID that you are looking for is taken.";
		softAssertion.assertEquals(actualErrorMessage, expectedErrorMessage);
		//Assert.assertEquals(actualErrorMessage, expectedErrorMessage);
		
		logger.debug("Hello");
		//softAssertion.assertTrue(false);
		
		softAssertion.assertAll();
		logger.debug("Validating of Create account with email id");
		}catch(Exception e) {
			e.printStackTrace();
			captureScreenshot();
			logger.error(e.toString());
			softAssertion.assertAll();
		}
	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}

}
